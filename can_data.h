#ifndef CAN_DATA_H
#define CAN_DATA_H

#include <stdint.h>

#define CAN_DATA_VALID_MAX_TIME    (10000)

struct VehicleBasicInfo{
/** Timems(系统时间)表示系统时间，以1ms为最小单位
	用途：用于检测当前共享数据最后的更新时间，当超过预定的阈值时，认为数据无效
	Timems :: = INTEGER(0 .. 0xFFFFFFFFFFFFFFFF)
	-- LSB of 1ms
	-- The value of 0 is unavailable	
	-- 8 byte
*/	uint64_t   time_ms;
/** VehicleType（车辆类型）是以车辆尺寸为基准的分类列表，许多路政单位会根据该列表进行数据收集和播报。
VehicleType ::= ENUMERATED {
None (0), -- Not Equipped, Not known or unavailable
Unknown (1), -- Does not fit any other category
Special (2), -- Special use
Moto (3), -- Motorcycle
Car (4), -- Passenger car
CarOther (5), -- Four tire single units
Bus (6), -- Buses
AxleCnt2 (7), -- Two axle, six tire single units
AxleCnt3 (8), -- Three axle, single units
AxleCnt4 (9), -- Four or more axle, single unit
AxleCnt4Trailer (10), -- Four or less axle, single trailer
AxleCnt5Trailer (11), -- Five or less axle, single trailer
AxleCnt6Trailer (12), -- Six or more axle, single trailer
AxleCnt5MultiTrailer (13), -- Five or less axle, multi-trailer
AxleCnt6MultiTrailer (14), -- Six axle, multi-trailer
AxleCnt7MultiTrailer (15), -- Seven or more axle, multi-trailer
...
} -- 1 byte
*/	uint8_t   type;
/** VehicleWidth(车辆宽度)表示汽车宽度数据值， 以1m为最小单位
	Width :: = FLOAT(0 - 10.23)
-- LSB of 1m
	-- A range of 0 to 10.23m
-- 4 byte
*/	double   width;
/** VehicleLength(车辆长度)表示汽车长度数据值，以1m为最小单位
	Length :: = FLOAT(0 .. 40.95)
-- LSB of 1m
	-- A range of 0 to 40.95m
-- 4 byte
*/	double   length;

/** Velocity（速度）指目标节点的行驶速度，以 1km/h 为单位,精确到小数点后三位，若速度无效则值为+589.725。
Velocity ::= FLOAT (0.. 589.725)
-- Units of 1km/h
-- The value +589.725 indicates that velocity is unavailable
-- 4 byte
*/	double   velocity;
/** Angle（角度）用来描述一个以“度”为单位的测量角，也可以用来表示目标节点的运动方向，即相对于
正北方向（或指定某一基准方向） 的以1 度为单位的顺时针夹角，精确到小数点后三位，即0.001。数值360.000 表示角度无效。
Angle ::= FLOAT (0 .. 360.000)
-- LSB of 1 degrees
-- The value 360.000 indicates  that angle is unavailable
-- 4 byte
*/	double   angle;
/** RotationSpeed(转速)指发动机的转速，以1rpm为单位，若转速无效，则值为0 
RotationSpeed ::= INTEGER(0, 8000)
	-- Units of 1 rpm
	-- The value 0 indicates that velocity is unavailable
	-- 2 byte
*/	uint16_t   rotationspeed;
/** GasPedalPos(油门踏板位置)指油门踏板踩下的百分比，以1%为最小单位，其中0%为未踩下油门踏板
	GasPedalPos :: = INTEGER(0, 100)
	--  Units of 1%
	-- The vlaue of 0 indicates that GasPedalPos is unavailable
	-- 1 byte
*/	uint8_t   gaspedalpos;
/** Totalmileage(总里程数)表示汽车行驶的总里程数，以1km为最小单位，0为无效值
	Totalmileage :: = INTEGER(0 .. 4294967295)
-- Units of 1km
-- The value 0 indicates that Totalmileage is unavailable
-- 4 byte
*/	uint32_t   totalmileage;
/** LeftOil(剩余油量)表示汽车剩余油量的百分之值，以1%为最小单位，0为无效值
	LeftOil :: = INTEGER(0, 100)
-- Units of 1 %
-- The value 0 indicates that LeftOil is unavailable
-- 1 byte
*/	uint8_t   leftoil;
/** Coolanttempeture(冷却液温度)表示汽车冷却液温度，以1摄氏度为最小单位，0为无效值
	Coolanttempeture ::= INTEGER(0,256)
	-- Uint of 1 。C
	-- The value 0 indicates that Coolanttempeture is unavailable
	-- 1 byte
*/	uint8_t   coolanttemp;
/** KeyState(钥匙状态)表示汽车钥匙状态，0表示汽车钥匙状态为无效状态，1表示汽车钥匙状态为LOCK状态，2表示汽车钥匙状态为ACC状态，3表示汽车钥匙状态为ON状态。
	KeyState :: = ENUMERATED
{
	0,	--	Invalid
		1,	--	Lock
		2,	--	ACC
		3,	--	ON
}-- 1 byte
*/	uint8_t   keystate;
/** HandleBrakeState(手刹状态值)表示车辆手刹状态值, 1代表拉起手刹，0表示手刹为拉起。
	HandleBrakeState := ENUMERATED 
	{ 
0,	--	off(手刹未拉起)
1,	--	on(手刹拉起)
}-- 1 byte
*/	uint8_t   handbrakestate;
/** VehiclebrakeState(刹车踏板状态)指刹车踏板状态，其中0表示刹车踏板状态无效或者未刹车，1表示刹车踏板已踩下。
VehicleBrakeState :: = ENUMERATED
{
0, --	vehicle brake off(未刹车)
1, --	vehicle brake on(刹车)
} -- 1 byte
*/	uint8_t   brakestate;
/** TransmissionState（变速箱状态）表示车辆挡位状态，主要提供当前车辆的变速箱状态数据，且以本车为参考系，0表示无效状态。
TransmissionState ::= ENUMERATED
{
	0,	--	Invalid(无效状态)
	1,	--	Park(停车挡)
	2,	--	Reverse(倒挡)
	3,	--	Neutral(空挡)
	4,	--	Drive(前进挡)
} -– 1 byte
*/	uint8_t   gear;
/** VehicleLightState(车辆灯光状态)中的每1位代表一类灯光的状态, 灯光开启为1,灯光关闭为0,各灯光状态位之间相互独立。
	VehicleLightState ::= BIT STRING{
		Lowbeamheadlight		(0),
		--(‘00000000 00000000 00000000 00000001’B)
		-- 近光灯状态
Highbeamheadlight		(1),
		--(‘00000000 00000000 00000000 00000010’B)
		-- 远光灯状态
Leftturnsignallight	(2),
		--(‘00000000 00000000 00000000 00000100’B)
		-- 左转向灯状态
Rightturnsignallight	(3),
		--(‘00000000 00000000 00000000 00001000’B)
		-- 右转向灯状态
		……
	}-- 4 byte
*/	uint32_t   lightstate;
/** VehicleDoorstatues(车门状态)中的每1位代表1类车门状态, 车门开启为1,车门关闭未0, 各车门状态位之间都相互独立。
VehicleDoorStatues :: = BIT STRING{
	Leftfrontdoor		(0),
						-- (‘00000000 00000000 00000000 00000001’B)
						-- 左前门状态
	Leftreardoor		(1),
						-- (‘00000000 00000000 00000000 00000010’B)
						-- 左后门状态
	rightfrontdoor		(2),
						-- (‘00000000 00000000 00000000 00000100’B)
						-- 右前门状态
rightreardoor		(3),
					-- (‘00000000 00000000 00000000 00001000’B)
					-- 右后门状态
BehindTrunkstate	（4）
					-- (‘00000000 00000000 00000000 00010000’B)
					-- 后备箱状态
......
}-- 4 byte
*/	uint32_t   doorstate;
/** SafetyBeltStatues (安全带状态)中每1位代表1种类型安全带状态，系上为1，未系上为0，各安全带状态位之间相互独立。
SafetyBeltStatues ::= BIT STRING {
Maindrivingsafetybelt 		(0),	
						--(‘00000000 00000000 00000000 00000001’B)
						--	主驾安全带状态
Copilotsafetybelt 			Copilotsafetybelt,		-- 副驾安全带状态
						--(‘00000000 00000000 00000000 00000010’B)
......
} -- 4 byte
*/	uint32_t   safetybelt;
};

#endif
