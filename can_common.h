#ifndef CAN_COMMON_H
#define CAN_COMMON_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include <sys/types.h>
#include <string.h>
#include <errno.h>

/* 各接口函数的返回值定义 */
typedef enum
{
    RET_OK     = 0,
    RET_ERROR,
    RET_E_MSG,
    RET_E_SEM,
    RET_E_SHM,
    RET_E_THREAD,
    RET_E_TIMEOUT,
}MODULE_RET;

/*******************************************************/

#define CAN_DEBUG

#define CAN_DEBUG_INFO           0x03
#define CAN_DEBUG_WARNING        0x02
#define CAN_DEBUG_ERROR          0x01

#define CAN_DEBUG_LEVEL          (CAN_DEBUG_INFO)

#ifdef CAN_DEBUG
#define can_log(level, x)        do { if (level <= CAN_DEBUG_LEVEL)       \
    {printf("[%s][%s][L:%d]:","CAN",__FUNCTION__,__LINE__); printf x;  \
  }}while (0)
#else
#define can_log(level, x)
#endif

#endif