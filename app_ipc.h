#ifndef APP_IPC_H
#define APP_IPC_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/shm.h>

#define APP_IPC_MSG
#define APP_IPC_SEM
#define APP_IPC_SHM

#ifdef APP_IPC_MSG

#define MSGBUF_SIZE        (10)
#define MSGTYPE_CANCMD     (1)
#define MSGTYPE_CANACK     (2)
#define MSGTYPE_REPORT     (3)
#define MSGTYPE_REPORTACK  (4)
#define MSGTYPE_GPSCMD     (5)
#define MSGTYPE_GPSACK     (6)

#define STR_UNKNOWN        ("unknown")
#define STR_OK             ("ok")
#define STR_INIT           ("init")
#define STR_OPEN           ("open")
#define STR_CLOSE          ("close")
#define STR_DEINIT         ("deinit")
#define STR_STATE          ("state")
#define STR_NOMSGS         ("nomsgs")
#define STR_MSGS           ("msgs")
#define STR_LIST           ("list")
#define STR_CHECK          ("check")
#define STR_UNCHECK        ("uncheck")

struct msgpack
{
	long msgtype;
	char ctrlstr[MSGBUF_SIZE];
};

#define MSG_PERM      (0660)
#define MSG_TIMEOUT   (10000)   //1ms for one

#define CAN_MSGKEY_PATH   (".")
#define CAN_MSGKEY_ID     ('A')

#define GPS_MSGKEY_PATH   (".")
#define GPS_MSGKEY_ID     ('A')

uint8_t app_msg_key(int* pmsgid,const char* pathname,int proj_id);
uint8_t app_msg_init(const char* pathname,int proj_id);
uint8_t app_msg_deinit(int msgid);
uint8_t app_msg_snd(int msgid,long type,const void* msgp,size_t msgsz);
uint8_t app_msg_rcv(int msgid,long type,void* msgp,size_t msgsz,int timeout);

#endif

/****************** 信号量相关 *********************************/

#ifdef APP_IPC_SEM

#define SEM_PERM       (0666)

#define CAN_SEMKEY_PATH    (".")
#define CAN_SEMKEY_ID      ('B')

#define GPS_SEMKEY_PATH    (".")
#define GPS_SEMKEY_ID      ('E')

uint8_t app_sem_key(int* psemid,const char* pathname,int proj_id);
uint8_t app_sem_init(const char* pathname,int proj_id);
uint8_t app_sem_lock(int semid);
uint8_t app_sem_unlock(int semid);
uint8_t app_sem_deinit(int semid);

#endif

/****************** 共享内存相关 *********************************/
#ifdef APP_IPC_SHM

#define SHM_PERM       (0666)

#define CAN_SHMKEY_PATH    (".")
#define CAN_SHMKEY_ID      ('C')

#define GPS_SHMKEY_PATH    (".")
#define GPS_SHMKEY_ID      ('D')

uint8_t app_shm_key(int* pshmid,void** pshmaddr,const char* pathname,int proj_id);
uint8_t app_shm_init(int size,void** shmaddr,const char* pathname,int proj_id);
uint8_t app_shm_deinit(int shmid,void* shmaddr);

#endif

#endif
