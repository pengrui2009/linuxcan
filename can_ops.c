#include "can_ops.h"

#define CMD_LENGTH    (100)
int can_init(int chl,int bps)
{
	char cmd[CMD_LENGTH];
	
	memset(cmd,0,sizeof(cmd));
	sprintf(cmd,"ip link set can%d down",chl);
	system(cmd);
	memset(cmd,0,sizeof(cmd));
	sprintf(cmd,"ip link set can%d type can bitrate %d",chl,bps);
	system(cmd);
	memset(cmd,0,sizeof(cmd));
	sprintf(cmd,"ip link set can%d up",chl);
	system(cmd);
	
	return 0;
}
int can_get_socket(void)
{
	return socket(PF_CAN,SOCK_RAW,CAN_RAW);
}
int can_bind_socket(int socket,int chl)
{
	int ret;
	struct ifreq ifr;
	struct sockaddr_can addr;
	
	ret = 0;
	if(socket == -1)
	{
		ret = -1;
	}
	if(ret == 0)
	{
		memset((void*)(&ifr),0,sizeof(struct ifreq));
		memset((void*)(&addr),0,sizeof(struct sockaddr_can));
		sprintf(ifr.ifr_name,"can%d",chl);
		
		if(-1 == ioctl(socket,SIOCGIFINDEX,&ifr))
		{
			ret = -1;
		}
		else
		{
			addr.can_family = AF_CAN;
			addr.can_ifindex = ifr.ifr_ifindex;
			if(-1  == bind(socket,(struct sockaddr*)&addr,sizeof(addr)))
			{
				ret = -1;
			}
		}
	}
	
	return ret;
}

/* set can filter 
	0:success    
	-1:error
*/
int can_set_socketfilter(int socket,int id,int mask)
{
	int ret;
	struct can_filter rfilter;
	
	ret = 0;
	if(socket == -1)
	{
		ret = -1;
	}
	else 
	{
		rfilter.can_id   = id;
		rfilter.can_mask = mask;
			
		ret =setsockopt(socket, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));
	}
	
	return ret;
}
/* set read timeout 
	0:success    
	-1:error
*/
int can_set_readtimeout(int socket,int t_ms)
{
	int ret;
	
	ret = 0;
	if(socket == -1)
	{
		ret = -1;
	}
	else
	{
		struct timeval timeout; 
		timeout.tv_sec = t_ms / 1000;
		timeout.tv_usec = (t_ms % 1000)*1000;
		
		ret = setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(struct timeval));
	}
	return ret;
}
/*	socket read
	>=0  : success  return the number of bytes read
	-1   : error
*/
int can_read_msg(int socket,struct can_frame* frame)
{
	int ret;
	
	ret = 0;
	if((socket == -1)||(frame == 0))
	{
		ret = -1;
	}
	else
	{
		ret = read(socket, frame, sizeof(struct can_frame));	
	}
	return ret;
}
/*	socket write
	>=0  : success   return the number of bytes write
	-1   : error
*/int can_write_msg(int socket,struct can_frame* frame)
{
	int ret;
	
	ret = 0;
	if((socket == -1)||(frame == 0))
	{
		ret = -1;
	}
	else
	{
		ret = write(socket, frame, sizeof(struct can_frame));
	}
	return ret;
}
	