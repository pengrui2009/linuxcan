#include "can_listener.h"

#define VERSION_MAIN      (0)
#define VERSION_SUB1      (0)
#define VERSION_SUB2      (3)

/* ģ��״̬���� */
typedef enum
{
    STATUS_DEINIT = 0,
    STATUS_INIT,
    STATUS_OPEN
}MCAN_STATUS;

int can_msg_id = -1;
int can_users = 0;

int main(void)
{	
	int ret;
	MCAN_STATUS module_status;
	int i;
	char msg[MSGBUF_SIZE];
	
	printf("linux can project V_%d.%d.%d\r\n",VERSION_MAIN,VERSION_SUB1,VERSION_SUB2);
	module_status = STATUS_DEINIT;
	ret = app_msg_init(CAN_MSGKEY_PATH,CAN_MSGKEY_ID);
	if(ret == RET_OK)
	{
		ret = app_msg_key(&can_msg_id,CAN_MSGKEY_PATH,CAN_MSGKEY_ID);
	}
	if(ret == RET_OK)
	{
		ret = can_data_init();
	}
	if(ret == RET_OK)
	{
		/* init bus interface */
		can_init(0,500000);
		
		module_status = STATUS_INIT;
//		ret = app_msg_snd(can_msg_it,MSGTYPE_ACK,STR_INIT,strlen(STR_INIT));
		can_log(CAN_DEBUG_INFO,("can process init ok\r\n"));
	}
	while(ret == RET_OK)
	{
		memset(msg,0,sizeof(msg));
		/* get the event */
		ret = app_msg_rcv(can_msg_id,MSGTYPE_CANCMD,msg,sizeof(msg),-1);
		if(RET_OK == ret)
		{
			if(memcmp(msg,STR_OPEN,strlen(STR_OPEN))==0)
			{
				if(module_status == STATUS_INIT)
				{
					/* create can interface socket */
					for(i=0;i<size_can0;i++)
					{
						can0_msgs[i].ctl = 0;
						if(0 != pthread_create(&(can0_msgs[i].pid),NULL,(void*)*can_lisener,(void*)(&can0_msgs[i])))
						{
							can_log(CAN_DEBUG_ERROR,("thread create error\r\n"));
							ret = RET_E_THREAD;
						}
					}
					if(ret == RET_OK)
					{
						module_status = STATUS_OPEN;
					}
				}
				if(module_status == STATUS_OPEN)
				{
					can_users++;
					ret = app_msg_snd(can_msg_id,MSGTYPE_CANACK,STR_OPEN,strlen(STR_OPEN));
				}
			}
			else if(memcmp(msg,STR_CLOSE,strlen(STR_CLOSE))==0)
			{
				if(module_status == STATUS_OPEN)
				{
					can_users--;
					if(can_users == 0)
					{
						/* close bus clear listeners */
						for(i=0;i<size_can0;i++)
						{
							can0_msgs[i].ctl = 0;
						}
						for(i=0;i<size_can0;i++)
						{
							if(0 != pthread_join(can0_msgs[i].pid,NULL))
							{
								can_log(CAN_DEBUG_ERROR,("thread exit error\r\n"));
								ret = RET_E_THREAD;
							}
						}
						if(ret == RET_OK)
						{
							module_status = STATUS_INIT;
						}
					}
				}
				if((module_status == STATUS_INIT)||(can_users>0))
				{
					ret = app_msg_snd(can_msg_id,MSGTYPE_CANACK,STR_CLOSE,strlen(STR_CLOSE));
				}
			}
//			else if(memcmp(msg,STR_DEINIT,strlen(STR_DEINIT))==0)
//			{
//				break;
//			}
			else if(memcmp(msg,STR_STATE,strlen(STR_STATE))==0)
			{
				if(can0_check_standby() == 1)
				{
					ret = app_msg_snd(can_msg_id,MSGTYPE_CANACK,STR_NOMSGS,strlen(STR_NOMSGS));
				}
				else
				{
					ret = app_msg_snd(can_msg_id,MSGTYPE_CANACK,STR_MSGS,strlen(STR_MSGS));
				}
				
			}
			else if(memcmp(msg,STR_LIST,strlen(STR_LIST))==0)
			{
				can0_datalist();
				ret = app_msg_snd(can_msg_id,MSGTYPE_CANACK,STR_LIST,strlen(STR_LIST));
			}
			else
			{
				can_log(CAN_DEBUG_ERROR,("msg undefined\r\n"));
				ret = app_msg_snd(can_msg_id,MSGTYPE_CANACK,STR_UNKNOWN,strlen(STR_UNKNOWN));
			}
		}
	}
	can_data_deinit();
	module_status = STATUS_DEINIT;
//	ret = app_msg_snd(can_msg_id,MSGTYPE_CANACK,STR_DEINIT,strlen(STR_DEINIT));
	can_log(CAN_DEBUG_INFO,("can process end\r\n"));
	return 0;
}
