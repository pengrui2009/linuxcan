#include "can_common.h"
#include "app_ipc.h"
#include "can_data.h"


uint8_t g_u8_can_module_init(void)
{
	uint8_t ret;
	int can_msg_id;

	ret = app_msg_key(&can_msg_id,CAN_MSGKEY_PATH,CAN_MSGKEY_ID);
	if(ret == RET_OK)
	{
		system("/home/root/can_listener &");
	}
//	if(ret == RET_OK)
//	{
//		char msg[MSGBUF_SIZE];
//    
//		memset(msg,0,sizeof(msg));
//		ret = app_msg_rcv(can_msg_id,MSGTYPE_CANACK,msg,sizeof(msg),MSG_TIMEOUT);
//		if(ret == RET_OK)
//		{
//			if(memcmp(msg,STR_INIT,strlen(STR_INIT))!=0)
//			{
//				ret = RET_E_MSG;
//			}
//		}
//	}
	
	return ret;
}
uint8_t g_u8_can_module_open(void)
{
	uint8_t ret;	
	int can_msg_id;

	ret = app_msg_key(&can_msg_id,CAN_MSGKEY_PATH,CAN_MSGKEY_ID);
	if(ret == RET_OK)
	{
		ret = app_msg_snd(can_msg_id,MSGTYPE_CANCMD,STR_OPEN,strlen(STR_OPEN));
	}
	if(ret == RET_OK)
	{
		char msg[MSGBUF_SIZE];
		
		memset(msg,0,sizeof(msg));
		ret = app_msg_rcv(can_msg_id,MSGTYPE_CANACK,msg,sizeof(msg),MSG_TIMEOUT);
		if(ret == RET_OK)
		{
			if(memcmp(msg,STR_OPEN,strlen(STR_OPEN))!=0)
			{
				ret = RET_E_MSG;
			}
		}
	}
	return ret;
}
uint8_t g_u8_can_module_close(void)
{
	uint8_t ret;
	int can_msg_id;

	ret = app_msg_key(&can_msg_id,CAN_MSGKEY_PATH,CAN_MSGKEY_ID);
	if(ret == RET_OK)
	{
		ret = app_msg_snd(can_msg_id,MSGTYPE_CANCMD,STR_CLOSE,strlen(STR_CLOSE));
	}
	if(ret == RET_OK)
	{
		char msg[MSGBUF_SIZE];
		
		memset(msg,0,sizeof(msg));
		ret = app_msg_rcv(can_msg_id,MSGTYPE_CANACK,msg,sizeof(msg),MSG_TIMEOUT);
		if(ret == RET_OK)
		{
			if(memcmp(msg,STR_CLOSE,strlen(STR_CLOSE))!=0)
			{
				ret = RET_E_MSG;
			}
		}
	}
	return ret;
}
uint8_t g_u8_can_module_deinit(void)
{
	uint8_t ret;
	int can_msg_id;

	ret = app_msg_key(&can_msg_id,CAN_MSGKEY_PATH,CAN_MSGKEY_ID);
	if(ret == RET_OK)
	{
		ret = app_msg_snd(can_msg_id,MSGTYPE_CANCMD,STR_DEINIT,strlen(STR_DEINIT));
	}
	if(ret == RET_OK)
	{
		char msg[MSGBUF_SIZE];
		
		memset(msg,0,sizeof(msg));
		ret = app_msg_rcv(can_msg_id,MSGTYPE_CANACK,msg,sizeof(msg),MSG_TIMEOUT);
		if(ret == RET_OK)
		{
			if(memcmp(msg,STR_DEINIT,strlen(STR_DEINIT))!=0)
			{
				ret = RET_E_MSG;
			}
			else
			{
				ret = app_msg_deinit(can_msg_id);
			}
		}
	}
	return ret;
}
/*
	check valid messages in can bus 
	
	RET_OK: no messages   other: hava valid messages
*/
uint8_t g_u8_can_module_standby(uint8_t* busstate)
{
	uint8_t ret;
	int can_msg_id;

	ret = app_msg_key(&can_msg_id,CAN_MSGKEY_PATH,CAN_MSGKEY_ID);
	if(ret == RET_OK)
	{
		ret = app_msg_snd(can_msg_id,MSGTYPE_CANCMD,STR_STATE,strlen(STR_STATE));
	}
	if(ret == RET_OK)
	{
		char msg[MSGBUF_SIZE];
		
		memset(msg,0,sizeof(msg));
		ret = app_msg_rcv(can_msg_id,MSGTYPE_CANACK,msg,sizeof(msg),MSG_TIMEOUT);
		if(ret == RET_OK)
		{
			if(memcmp(msg,STR_NOMSGS,strlen(STR_NOMSGS))==0)
			{
				*busstate = 1;
			}
			else if(memcmp(msg,STR_MSGS,strlen(STR_MSGS))==0)
			{
				*busstate = 0;
			}
			else
			{
				ret = RET_E_MSG;
			}
		}
	}
	return ret;
}
uint8_t g_u8_can_module_datalist(void)
{
	uint8_t ret;
	int can_msg_id;

	ret = app_msg_key(&can_msg_id,CAN_MSGKEY_PATH,CAN_MSGKEY_ID);
	if(ret == RET_OK)
	{
		ret = app_msg_snd(can_msg_id,MSGTYPE_CANCMD,STR_LIST,strlen(STR_LIST));
	}
	if(ret == RET_OK)
	{
		char msg[MSGBUF_SIZE];
		
		memset(msg,0,sizeof(msg));
		ret = app_msg_rcv(can_msg_id,MSGTYPE_CANACK,msg,sizeof(msg),MSG_TIMEOUT);
		if(ret == RET_OK)
		{
			if(memcmp(msg,STR_LIST,strlen(STR_LIST))!=0)
			{
				ret = RET_E_MSG;
			}
		}
	}
	return ret;
}

uint8_t g_u8_can_module_getdata(struct VehicleBasicInfo* pdata)
{
	uint8_t ret,ret1;
	int can_shm_id;
	struct VehicleBasicInfo* shmaddr;
	int can_sem_id;
	
	ret = app_shm_key(&can_shm_id,((void*)(&shmaddr)),CAN_SHMKEY_PATH,CAN_SHMKEY_ID);
	if(ret == RET_OK)
	{
		ret = app_shm_key(&can_sem_id,(void**)(&shmaddr),CAN_SEMKEY_PATH,CAN_SEMKEY_ID);
	}
	if(ret == RET_OK)
	{
		ret = app_sem_lock(can_sem_id);
	}
	if(ret == RET_OK)
	{
		if((pdata != NULL)&&(shmaddr != NULL))
		{
			memcpy(pdata,shmaddr,sizeof(struct VehicleBasicInfo));
		}
		else
		{
			ret = RET_ERROR;
		}
		ret1 = app_sem_unlock(can_sem_id);
	}

	if(ret == RET_OK)
	{
		ret = ret1;
	}

	return ret;
}
/****************************************************************************/
int main(int argc,char* argv[])
{
	uint8_t  ret;
	
	ret = RET_ERROR;
	if(argc != 2)
	{
		printf("parameter error\r\n");
		ret = RET_ERROR;
	}
	else if(memcmp(argv[1],"init",strlen("init"))==0)
	{
		ret = g_u8_can_module_init();
	}	
	else if(memcmp(argv[1],"open",strlen("open"))==0)
	{
		ret = g_u8_can_module_open();
	}	
	else if(memcmp(argv[1],"close",strlen("close"))==0)
	{
		ret = g_u8_can_module_close();
	}
	else if(memcmp(argv[1],"deinit",strlen("deinit"))==0)
	{
		ret = g_u8_can_module_deinit();
	}
	else if(memcmp(argv[1],"state",strlen("state"))==0)
	{
		uint8_t state;
		
		state = 0;
		ret = g_u8_can_module_standby(&state);
		printf("can bus state : %d\r\n",state);
	}
	else if(memcmp(argv[1],"list",strlen("list"))==0)
	{
		ret = g_u8_can_module_datalist();
	}
	return ret;
}
