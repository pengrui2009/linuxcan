#ifndef CAN_OPS_H
#define CAN_OPS_H

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/if.h>
#include <linux/can/raw.h>
#include <errno.h>
#include <time.h>

int can_init(int chl,int bps);
int can_get_socket(void);
int can_bind_socket(int socket,int chl);
int can_set_socketfilter(int socket,int id,int mask);
int can_set_readtimeout(int socket,int t_ms);
int can_read_msg(int socket,struct can_frame* frame);
int can_write_msg(int socket,struct can_frame* frame);
	
#endif
