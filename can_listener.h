#ifndef CAN_LISTENER_H
#define CAN_LISTENER_H

#include "can_common.h"
#include "can_ops.h"
#include "app_ipc.h"
#include <pthread.h>
#include "can_data.h"

/* 数据类型定义 */
typedef void (*pfunc)(uint8_t* pbuf);

/* 模块通信结构定义 */
struct msgitem
{
	pthread_t      pid;
	uint8_t        ctl;   //  0  线程未初始化完成   1 线程初始化完成
    uint8_t        state; //  0  该CAN报文超时      1 该CAN报文数据及时有效
	uint32_t       id;
    uint32_t       mask;
    uint32_t       timeout;
    pfunc          func;
};

void* can_lisener(void *arg);

extern struct msgitem can0_msgs[];
extern int size_can0;

uint8_t can_data_init(void);
uint8_t can_data_deinit(void);
uint8_t can_data_get(struct VehicleBasicInfo* pdata);


/*******************************************************/
#endif
