.PHONY: clean all

DIR_OBJS = objs
DIR_DEPS = deps
DIRS = $(DIR_OBJS) $(DIR_DEPS)

CC = arm-linux-gnueabihf-gcc
CFLAGS = -pthread

EXE  = can_listener

SRCS = $(wildcard *.c) 
OBJS = $(patsubst %.c,%.o,$(SRCS))
OBJS := $(addprefix $(DIR_OBJS)/,$(OBJS))
DEPS = $(SRCS:.c=.dep)
DEPS := $(addprefix $(DIR_DEPS)/,$(DEPS))

ifeq ("$(wildcard $(DIR_OBJS))","")
DEP_DIR_OBJS := $(DIR_OBJS)
endif
ifeq ("$(wildcard $(DIR_DEPS))","")
DEP_DIR_DEPS := $(DIR_DEPS)
endif

all:$(EXE)

ifneq ($(MAKECMDGOALS),clean)
include $(DEPS)
endif

$(DIRS):
	mkdir $@
$(EXE):$(filter-out %test.o,$(OBJS))
	$(CC) $(CFLAGS) -o $@ $(filter %.o,$^)
$(DIR_OBJS)/%.o:$(DEP_DIR_OBJS) %.c
	$(CC) $(CFLAGS) -o $@ -c $(filter %.c,$^)
$(DIR_DEPS)/%.dep:$(DEP_DIR_DEPS) %.c
	@echo "creating $@ ..."
	@set -e; \
	rm -fr $@.tmp ; \
	$(CC) -E -MM $(filter %.c,$^) > $@.tmp ; \
	sed 's,\(.*\)\.o[ :]*,objs/\1.o $@: ,g' < $@.tmp > $@ ; \
	rm -fr $@.tmp


clean:
	rm -fr $(EXE) $(TEST) $(DIRS)

test:objs/test.o objs/app_ipc.o 
	$(CC) $(CFLAGS) -o $@ $^


