#include "app_ipc.h"

/* 各接口函数的返回值定义 */
typedef enum
{
    RET_OK     = 0,
    RET_ERROR,
    RET_E_MSG,
    RET_E_SEM,
    RET_E_SHM,
    RET_E_THREAD,
    RET_E_TIMEOUT,
}MODULE_RET;

#define  MSG_READ_DELAY      (1000)   //unit: 1us
/****************** 消息队列相关 *********************************/
#ifdef APP_IPC_MSG
uint8_t app_msg_key(int* pmsgid,const char* pathname,int proj_id)
{
	uint8_t ret;
	key_t msgkey;
	
	ret = RET_OK;
	if(pmsgid == NULL)
	{
		ret = RET_ERROR;
	}
	/* get key */
	if(ret == RET_OK)
	{
		if((msgkey = ftok(pathname,proj_id)) == -1)
		{
			printf("ftok error\r\n");
			ret = RET_E_MSG;
		}
	}
	if(ret == RET_OK)
	{
		if((*pmsgid = msgget(msgkey,MSG_PERM))==-1)
		{
			printf("msgget error\r\n");
			ret = RET_E_MSG;
		}
	}

	return ret;
}
uint8_t app_msg_init(const char* pathname,int proj_id)
{
	uint8_t ret;
	key_t msgkey;
	
	ret = RET_OK;
	/* get key */
	if((msgkey = ftok(pathname,proj_id)) == -1)
	{
		printf("ftok error\r\n");
		ret = RET_E_MSG;
	}
	if(ret == RET_OK)
	{
		if(msgget(msgkey,MSG_PERM|IPC_CREAT)==-1)//|IPC_EXCL
		{
			printf("msgget error\r\n");
			ret = RET_E_MSG;
		}
	}
	return ret;
}
uint8_t app_msg_deinit(int msgid)
{
	uint8_t ret;
	
	ret = RET_OK;
	if(msgctl(msgid,IPC_RMID,0) != 0)
	{
		printf("msgctl error\r\n");
		ret = RET_E_MSG;
	}
	return ret;
}
uint8_t app_msg_snd(int msgid,long type,const void* msgp,size_t msgsz)
{
	uint8_t ret;
	void* pbuf;

	ret = RET_OK;
	pbuf = malloc(msgsz+sizeof(long));
	if(pbuf == NULL)
	{
		ret = RET_ERROR;
	}
	else
	{
		memset(pbuf,0,msgsz+sizeof(long));
		memcpy((pbuf+sizeof(long)),msgp,msgsz);
		*((long*)pbuf) = type;
		if(msgsnd(msgid,pbuf,msgsz,0)==-1)
		{
			printf("msgsnd error\r\n");
			ret = RET_E_MSG;
		}
	}
	if(ret == RET_OK)
	{
		printf("send msg[%ld]\r\n",type);
	}		
	free(pbuf);
	return ret;
}

uint8_t app_msg_rcv(int msgid,long type,void* msgp,size_t msgsz,int timeout)
{
	uint8_t ret;
	void* pbuf;
	int i;
	
	ret = RET_OK;	
	pbuf = malloc(msgsz+sizeof(long));
	if(pbuf == NULL)
	{
		ret = RET_ERROR;
	}
	else
	{
		memset(pbuf,0,msgsz+sizeof(long));
		*((long*)pbuf) = type;
		if(timeout == 0)
		{
			if(msgrcv(msgid,pbuf,msgsz,type,IPC_NOWAIT)==-1)
			{
				if(errno != ENOMSG)
				{
					printf("msgrcv error\r\n");
					ret = RET_E_MSG;
				}
			}
		}
		else if(timeout > 0)
		{
			for(i=0;i<timeout;i++)
			{
				if(msgrcv(msgid,pbuf,msgsz,type,IPC_NOWAIT)==-1)
				{
					if(errno != ENOMSG)
					{
						printf("msgrcv error\r\n");
						ret = RET_E_MSG;
						break;
					}
				}
				else
				{
					break;
				}
				if(0 != usleep(MSG_READ_DELAY))
				{
					printf("time delay error\r\n");
					ret = RET_ERROR;
					break;
				}
			}
			if((ret == RET_OK)&&(i>=timeout))
			{
				printf("msgrcv timeout\r\n");
				ret = RET_E_MSG;
			}
		}
		else
		{
			if(msgrcv(msgid,pbuf,msgsz,type,0)==-1)
			{
				printf("msgrcv error\r\n");
				ret = RET_E_MSG;
			}
		}
	}
	if(ret == RET_OK)
	{
		memcpy(msgp,(pbuf+sizeof(long)),msgsz);
		printf("get msg[%ld]\r\n",type);
	}
	free(pbuf);
	return ret;
}
#endif
/****************** 信号量相关 *********************************/
#ifdef APP_IPC_SEM
uint8_t app_sem_key(int* psemid,const char* pathname,int proj_id)
{
	uint8_t ret;
	key_t semkey;
	
	ret = RET_OK;
	if(psemid == NULL)
	{
		ret = RET_ERROR;
	}
	if(ret == RET_OK)
	{
		if((semkey = ftok(pathname,proj_id)) == -1)
		{
			printf("ftok error\r\n");
			ret = RET_E_SEM;
		}
	}
	if(ret == RET_OK)
	{
		if((*psemid = semget(semkey,1,SEM_PERM))==-1)
		{
			printf("semget error\r\n");
			ret = RET_E_SEM;
		}
	}
	return ret;
}
uint8_t app_sem_init(const char* pathname,int proj_id)
{
	uint8_t ret;
	int semid;
	key_t semkey;
	
	ret = RET_OK;
	semid = 0;
	/* get key */
	if((semkey = ftok(pathname,proj_id)) == -1)
	{
		printf("ftok error\r\n");
		ret = RET_E_SEM;
	}
	if(ret == RET_OK)
	{
		if((semid = semget(semkey,1,SEM_PERM|IPC_CREAT))==-1)//|IPC_EXCL
		{
			printf("semget error\r\n");
			ret = RET_E_SEM;
		}
	}
	if(ret == RET_OK)
	{
		union semun {
		   int              val;    /* Value for SETVAL */
		   struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
		   unsigned short  *array;  /* Array for GETALL, SETALL */
		   struct seminfo  *__buf;  /* Buffer for IPC_INFO
		                               (Linux-specific) */
		};
		union semun semopts;
		
		semopts.val = 1;
		if(semctl(semid,0,SETVAL,semopts)==-1)
		{
			printf("semget error\r\n");
			ret = RET_E_SEM;
		}
	}
	return ret;
}
uint8_t app_sem_lock(int semid)
{
	uint8_t ret;
	struct sembuf buf = {0,-1,0};
	
	ret = RET_OK;
	if(semop(semid,&buf,1)==-1)
	{
		printf("semop error\r\n");
		ret = RET_E_SEM;
	}

	return ret;
}
uint8_t app_sem_unlock(int semid)
{
	uint8_t ret;
	struct sembuf buf = {0,1,0};
	
	ret =  RET_OK;	
	if(semop(semid,&buf,1)==-1)
	{
		printf("semop error\r\n");
		ret = RET_E_SEM;
	}
	
	return ret;
}
uint8_t app_sem_deinit(int semid)
{
	uint8_t ret;
	
	ret =  RET_OK;
	if(semctl(semid,0,IPC_RMID)==-1)
	{
		printf("semctl error\r\n");
		ret = RET_E_SEM;
	}
	
	return ret;
}
#endif
/****************** 共享内存相关 *********************************/
#ifdef APP_IPC_SHM
uint8_t app_shm_key(int* pshmid,void** pshmaddr,const char* pathname,int proj_id)
{
	uint8_t ret;
	key_t shmkey;
	
	ret = RET_OK;
	if(pshmid == NULL)
	{
		ret = RET_ERROR;
	}
	if(ret == RET_OK)
	{
		if((shmkey = ftok(pathname,proj_id)) == -1)
		{
			printf("ftok error\r\n");
			ret = RET_E_SHM;
		}
	}
	if(ret == RET_OK)
	{
		if((*pshmid = shmget(shmkey,0,SHM_PERM))==-1)
		{
			printf("semget error\r\n");
			ret = RET_E_SHM;
		}
	}
	if(ret == RET_OK)
	{
		if((*pshmaddr = shmat(*pshmid,(void*)0,0))==(void*)-1)
		{
			printf("shmat error\r\n");
			ret = RET_E_SHM;
		}
	}

	return ret;
}
uint8_t app_shm_init(int size,void** shmaddr,const char* pathname,int proj_id)
{
	uint8_t ret;
	int shmid;
	key_t shmkey;
	
	ret = RET_OK;
	shmid = 0;
	/* get key */
	if((shmkey = ftok(pathname,proj_id)) == -1)
	{
		printf("ftok error\r\n");
		ret = RET_E_SHM;
	}
	if(ret == RET_OK)
	{
		if((shmid = shmget(shmkey,size,SHM_PERM|IPC_CREAT))==-1)//|IPC_EXCL
		{
			printf("semget error\r\n");
			ret = RET_E_SHM;
		}
	}
	if(ret == RET_OK)
	{
		if((*shmaddr = shmat(shmid,(void*)0,0))==(void*)-1)
		{
			printf("shmat error\r\n");
			ret = RET_E_SHM;
		}
	}
	return ret;
}
uint8_t app_shm_deinit(int shmid,void* shmaddr)
{
	uint8_t ret;
	
	ret =  RET_OK;
    if(shmdt(shmaddr) == -1)  
	{  
		printf("shmdt error\r\n");
		ret = RET_E_SHM;
	}  
	if(ret == RET_OK)
    { 
	    if(shmctl(shmid, IPC_RMID, 0) == -1)  
	    {  
	        printf("shmctl error\r\n");
			ret = RET_E_SHM;  
	    }
	}  
    
    return ret;
}
#endif

